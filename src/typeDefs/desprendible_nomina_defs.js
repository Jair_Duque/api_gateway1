const { gql } = require('apollo-server');

const desprendibleNominaTypeDefs = gql`
    type DesprendibleNomina {
        id: String!
        username: String!
        nombre1: String!
        nombre2: String!
        apellido1: String!
        apellido2: String!
        documento: String!
        credito: Float!
        salario: Float!
        fecha: String!
        salud: Float!
        pension: Float!
        devengado: Float!
        deduccion: Float!
        valorTotal: Float!
    }
    

    input DesprendibleNominaInput {
        username: String!
        nombre1: String!
        nombre2: String!
        apellido1: String!
        apellido2: String!
        documento: String!
        credito: Float!
        salario: Float!
    }

    extend type Query {
        desprendibleNominaByUsername(username:String!):[DesprendibleNomina]
    }

    extend type Mutation {
        createDesprendibleNomina(desprendibleNomina:DesprendibleNominaInput!):DesprendibleNomina
        deleteDesprendibleNomina(desprendibleNominaId:String!):String!
    }
`;

module.exports = desprendibleNominaTypeDefs;