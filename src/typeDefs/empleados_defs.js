const { gql } = require('apollo-server');

const empleadosTypeDefs = gql`
    type Empleado {
        id: Int!
        nombre1: String!
        nombre2: String!
        apellido1: String!
        apellido2: String!
        documento: Float!
        salario: Float!
        fechaini_contrato: String!
    }

    type Adelanto {
        id: Int!
        id_emp: Int!
        Valor: Float!
        fecha: String!
    }

    input EmpleadoInput {
        nombre1: String!
        nombre2: String!
        apellido1: String!
        apellido2: String!
        documento: String!
        salario: Float!
        fechaini_contrato: String!
    }

    input EmpleadoUpdateInput {
        empleadoId: Int!
        nombre1: String!
        nombre2: String!
        apellido1: String!
        apellido2: String!
        documento: String!
        salario: Float!
        fechaini_contrato: String!
    }

    input AdelantoInput {
        id_emp: Int!
        Valor: Float!
        fecha: String!
    }

    input AdelantoUpdateInput {
        adelantoId: Int!
        id_emp: Int!
        Valor: Float!
        fecha: String!
    }

    extend type Query {
        empleadoByIdd(idEmpleado:Int!):Empleado!
        adelantoByIdd(idAdelanto:Int!):Adelanto!
    }

    extend type Mutation {
        createEmpleado(empleado:EmpleadoInput):Empleado!  
        updateEmpleado(empleadoUpdateInput:EmpleadoUpdateInput):Empleado!  
        deleteEmpleado(id:Int!):String!
        createAdelanto(adelanto:AdelantoInput):Adelanto!
        updateAdelanto(adelantoUpdate:AdelantoUpdateInput):Adelanto!
        deleteAdelanto(id:Int!):String!
    }
`;

module.exports = empleadosTypeDefs;