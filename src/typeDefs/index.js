
//Se llama al typedef (esquema) de cada submodulo
const authTypeDefs = require('./auth_type_defs');
const desprendibleNominaTypeDefs = require('./desprendible_nomina_defs');
const empleadosTypeDefs = require('./empleados_defs');

//Se unen
const schemasArrays = [authTypeDefs, desprendibleNominaTypeDefs, empleadosTypeDefs];

//Se exportan
module.exports = schemasArrays;