const empleadosResolver = {
    Query: {
        empleadoByIdd: async (_, { idEmpleado }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
            if (usernameToken != null)
                return await dataSources.empleadosApi.getEmpleadoById(idEmpleado);
            else
                return null;
        },
        adelantoByIdd: async (_, { idAdelanto }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
            if (usernameToken != null) 
                return await dataSources.empleadosApi.getAdelantoById(idAdelanto);
            else
                return null;
        }
    },
    Mutation: {
        createEmpleado: async (_, { empleado }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
        
            if (usernameToken != null)
                return await dataSources.empleadosApi.createEmpleado(empleado)
            else
                return null;


        },
        updateEmpleado: async (_, { empleadoUpdateInput }, { dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username

            if (usernameToken != null)
                return await dataSources.empleadosApi.updateEmpleado(empleadoUpdateInput)
            else
                return null;

        },
        deleteEmpleado: async (_, { id }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username

            if (usernameToken != null)
                return await dataSources.empleadosApi.deleteEmpleado(id)
            else
                return null;

        },
        createAdelanto: async (_, { adelanto }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username

            if (usernameToken != null)
                return await dataSources.empleadosApi.createAdelanto(adelanto)
            else
                return null;

        },
        updateAdelanto: async (_, { adelantoUpdate }, { dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username

            if (usernameToken != null)
                return await dataSources.empleadosApi.updateAdelanto(adelantoUpdate)
            else
                return null;
        },
        deleteAdelanto: async (_, { id }, {dataSources, userIdToken}) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username

            if (usernameToken != null)
                return await dataSources.empleadosApi.deleteAdelanto(id)
            else
                return null;

        }
    }
}

module.exports = empleadosResolver;