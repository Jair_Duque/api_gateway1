const usersResolver = {
    Query: {
        userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
            if (userId == userIdToken)
                return dataSources.authApi.getUser(userId)
            else
                return null
        },
    },
    Mutation: {
        signUpUser: async (_, { userInput }, { dataSources }) => {
  
            const authInput = {
                username: userInput.username,
                password: userInput.password,
                firstName: userInput.firstName,
                lastName: userInput.lastName,
                email: userInput.email,
                document: userInput.document,
                lastChangeDate: userInput.lastChangeDate,
                phone: userInput.phone,
                address: userInput.address
            }
            return await dataSources.authApi.createUser(authInput);
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.authApi.authRequest(credentials),
        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.authApi.refreshToken(refresh),
    }
};
module.exports = usersResolver;