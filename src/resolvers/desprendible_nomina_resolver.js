const desprendibleNominaResolver = {
    Query: {
        desprendibleNominaByUsername: async(_, { username }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
            if (username == usernameToken) 
                return await dataSources.desprendibleNominaApi.getListByUsername(username);
            else
                return null;
        }
    },
    Mutation: {
        createDesprendibleNomina: async(_, { desprendibleNomina }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
            if (desprendibleNomina.username == usernameToken)
                return await dataSources.desprendibleNominaApi.createDesprendibleNomina(desprendibleNomina);
            else
                return null;

        },
        deleteDesprendibleNomina: async(_, { desprendibleNominaId }, { dataSources, userIdToken }) => {
            usernameToken = (await dataSources.authApi.getUser(userIdToken)).username
            if(usernameToken != null)
                return await dataSources.desprendibleNominaApi.deleteDesprendibleNomina(desprendibleNominaId);
            else
                return null;
        }
    }
}

module.exports = desprendibleNominaResolver;