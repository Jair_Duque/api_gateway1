const desprendibleNominaResolver = require('./desprendible_nomina_resolver');
const empleadosResolver = require('./empleados_resolver');
const authResolver = require('./auth_resolver');

const lodash = require('lodash');
const resolvers = lodash.merge(desprendibleNominaResolver, empleadosResolver, authResolver);

module.exports = resolvers;