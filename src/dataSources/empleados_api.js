const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class EmpleadosAPI extends RESTDataSource {
    
    constructor() {
        super();
        this.baseURL = serverConfig.empleados_api_url;
    }

    async getEmpleadoById(idEmpleado) {
        return await this.get(`/empleados/${idEmpleado}`);
    }

    async createEmpleado(empleado) {
        empleado = new Object(JSON.parse(JSON.stringify(empleado)));
        return await this.post(`/empleados/`, empleado);
    }

    async updateEmpleado(empleadoUpdateInput) {
        empleadoUpdateInput = new Object(JSON.parse(JSON.stringify(empleadoUpdateInput)));
        return await this.put(`/empleados/${empleadoUpdateInput.empleadoId}`, empleadoUpdateInput);
    }
    async deleteEmpleado(id) {
        return await this.delete(`/empleados/${id}`);
    }


    async getAdelantoById(idAdelanto) {
        return await this.get(`/adelanto/${idAdelanto}`);
    }

    async createAdelanto(adelanto) {
        adelanto = new Object(JSON.parse(JSON.stringify(adelanto)));
        return await this.post(`/adelanto/`, adelanto);
    }
    async updateAdelanto(adelantoUpdate) {
        adelantoUpdate = new Object(JSON.parse(JSON.stringify(adelantoUpdate)));
        return await this.put(`/adelanto/${adelantoUpdate.adelantoId}`, adelantoUpdate);
    }
    async deleteAdelanto(id) {
        return await this.delete(`/adelanto/${id}`);
    }
}
module.exports = EmpleadosAPI;