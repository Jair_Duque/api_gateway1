const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class DesprendibleNominaAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.desprendible_nomina_api_url;
    }

    async createDesprendibleNomina(desprendibleNomina) {
        desprendibleNomina = new Object(JSON.parse(JSON.stringify(desprendibleNomina)));
        return await this.post(`/DN/`, desprendibleNomina);
    }
    async getListByUsername(username) {
        return await this.get(`/DN/${username}/`);
    }
    async deleteDesprendibleNomina(desprendibleNominaID) {
        return await this.delete(`/DN/${desprendibleNominaID}/`);
    }
}
module.exports = DesprendibleNominaAPI;