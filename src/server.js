module.exports = {
    auth_api_url: 'https://mision-tic-auth-ms-be.herokuapp.com',
    desprendible_nomina_api_url: 'https://mision-tic-dnomina-ms-be.herokuapp.com',
    empleados_api_url: 'https://mision-tic-employees-ms-be.herokuapp.com'
    };